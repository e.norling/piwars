from line import LineFollower
from button import Button
from legomotors import Motors
import rpyc
import socket
import RPi.GPIO as GPIO
from triangula.util import IntervalCheck


left = LineFollower(17)     # 4 on glitterator, 23 on camjam
right = LineFollower(4)     # 17 on glitterator, 24 on camjam
centre = LineFollower(18)   # 18 on glitterator, 25 on camjam

mx = Motors(100, 1, 2, 3)      # = Motors(100, 1, 2) on glitterator

START_SPEED = 40
MAX_SPEED = 100

try:
    c = rpyc.connect("glitter.local", 18861)
except socket.gaierror as e:
    c = None
    
if not c is None:
    c.root.pulse("DarkGoldenRod")

check = False
# Emma's revisions to Peter's truth table
def gotChange(channel):
    """Callback function for when a change is detected under any of the
    followers
    """

    global check, speed, straight
    if (check):
        return; #something else is already responding to change

    check = True
    L = left.IsOverBlack()
    C = centre.IsOverBlack()
    R = right.IsOverBlack()
#    print "line sensors ", L, C, R
#    speed = START_SPEED # changed state, so drop back to start speed
    straight = False

    if (not L and not C and not R):
        #lost line - should do random walk?
        # for time being, just keep going as is
        pass
    elif (not L and C and not R or (L and C and R)):
        #line under centre only or all three - go forward
        speed = START_SPEED
        mx.forward(speed)
        straight = True
    elif (R):
        #line under right - turn right
        mx.drive_left(speed)
        mx.drive_right(-speed/2)
    elif (L):
        #line under left - turn left
        mx.drive_left(-speed/2)
        mx.drive_right(speed)
    check = False
  

def button_pressed(channel):
    """Callback to start/stop line following"""
    global running

    if (not running):
        print "turning line following on"
        if not c is None:
            c.root.pulse("DarkGreen")
        centre.add_event(gotChange)
        left.add_event(gotChange)
        right.add_event(gotChange)
        gotChange(1)

    else:
        print "turning line following off"
        if not c is None:
            c.root.pulse("DarkGoldenRod")
        mx.stop()
        centre.rem_events()
        left.rem_events()
        right.rem_events()

    running = not running #flip state of running

try:
    switch_pin = 27
    button = Button(switch_pin)
    running = False
    speed = START_SPEED
    straight = False

    mx.stop()
    # going to adjust speed at most 4 times per second
    padding = IntervalCheck(interval=0.25)
    
    button.add_event(button_pressed)
    print "Press button to start following lines"
    while True:
        with padding:
            if (running and not check and straight and (speed < MAX_SPEED)):
                speed = speed + 1
                mx.forward(speed)
##        pass

except KeyboardInterrupt:
  GPIO.cleanup()
