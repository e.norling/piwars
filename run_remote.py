from ps3controller import Controller
import RPi.GPIO as GPIO
import rpyc
import socket
import time
GPIO.setmode(GPIO.BCM)

time.sleep(10)

try:
    c = rpyc.connect("glitter.local", 18861)
except Exception as e:
    c = None
    
if not c is None:
    c.root.pulse("red")

# This bit of code assumes that you'll press the button when the controller
# has been paired

switch_pin = 27
motor_control_pin = 26

GPIO.setup(switch_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(26, GPIO.OUT)
GPIO.output(26, GPIO.LOW)
print "Press the button on the yellow brick if/when the PS3 controller has been paired"

paired = False
while (paired == False):
    if GPIO.input(switch_pin) == False:
        paired = True

if not c is None:
    c.close()
# Controller takes over lights from here

# You shouldn't have pressed the button without pairing the controller
# otherwise the next bit will fail

controller = None
try:
    controller = Controller(100, 1, 2, 3)

except KeyboardInterrupt:
    if not controller is None:
        controller.cleanup()
    GPIO.cleanup()
    if not c is None:
        c.close()
