from triangula.input import SixAxis, SixAxisResource
from triangula.util import IntervalCheck
from legomotors import Motors
import rpyc
import socket

running_flicker=False

colors = ["DarkGoldenRod", "crimson", "OrangeRed", "gold",
            "DarkGreen", "lawngreen", "aqua", "MediumBlue",
            "indigo", "DarkViolet", "DarkMagenta"]
color = 0
num_colors = len(colors)

functions = [lambda x: x.root.pulse(), lambda x: x.root.glitter(), lambda x: x.root.strobe(), lambda x: x.root.rainbow()]
function = 0
num_functions = len(functions)

class Controller:
    """
    Class to handle the PS3 controller.
    """

    MAX_SPEED = 100
    MIN_SPEED = 20

    max_speed = MAX_SPEED

    def __init__(self, frequency, left, right, flicker):
        """
        Discover and initialise a PS3 controller that is paired
        with this computer.
        :param int frequency:
            Frequency at which to initialise the motors
        :param int left:
            Socket left motor is connected to
        :param int right:
            Socket right motor is connected to
        """
        mx = Motors(frequency, left, right, flicker)
        try:
            self.connection = rpyc.connect("glitter.local", 18861)
        except socket.gaierror as e:
            self.connection = None

        # Get a joystick. This will fail unless the SixAxis controller is
        # paired and active.
        # The bind_defaults argument specifies that we should bind actions
        # to the SELECT and START buttons to centre the controller and reset
        # the calibration respectively.
        with SixAxisResource(bind_defaults=True) as joystick:
            if not self.connection is None:
                self.connection.root.pulse("DarkGoldenRod")
            delay = IntervalCheck(interval = 0.2)

            # Define what we want to happen when a button is pressed
            def slow(button):
                self.max_speed = max(self.max_speed*0.9,self.MIN_SPEED)
                #print ("Max speed is now %d"%(self.max_speed))

            # Define what we want to happen when a button is pressed
            def fast(button):
                self.max_speed = min(self.max_speed*1.1,self.MAX_SPEED)
                #print ("Max speed is now %d"%(self.max_speed))

            # Define what we want to happen when cross button is pressed
            def run_flicker(button):
                global running_flicker
                if (not running_flicker):
                    mx.flicker_forward(100)
                else:
                    mx.flicker_stop()
                running_flicker = not running_flicker

            # Define what we want to happen when square button is pressed
            def reverse_flicker(button):
                global running_flicker
                if (not running_flicker):
                    mx.flicker_forward(-100)
                else:
                    mx.flicker_stop()
                running_flicker = not running_flicker

            def change_color_up(button):
                global color, colors
                color = (color+1)%num_colors
                self.connection.root.set_color(colors[color])

            def change_color_down(button):
                global color, colors
                color = (color-1)%num_colors
                self.connection.root.set_color(colors[color])

            def change_function_up(button):
                global function, functions
                function = (function+1)%num_functions
                functions[function](self.connection)

            def change_function_down(button):
                global function, functions
                function = (function-1)%num_functions
                functions[function](self.connection)

            

            # Tie buttons to drive the winder forward/backward
            joystick.register_button_handler(run_flicker, SixAxis.BUTTON_CROSS)
            joystick.register_button_handler(reverse_flicker, SixAxis.BUTTON_SQUARE)
            # Tie buttons to increase/decrease the flat-out max speed
            joystick.register_button_handler(fast, SixAxis.BUTTON_TRIANGLE)
            joystick.register_button_handler(slow, SixAxis.BUTTON_CIRCLE)
    
            # If we have a connection to the lights manager,
            if not self.connection is None:
                # 1) Tie button to change color of LEDs
                joystick.register_button_handler(change_color_up,
                                                    SixAxis.BUTTON_D_UP)
                joystick.register_button_handler(change_color_down,
                                                    SixAxis.BUTTON_D_DOWN)
                # 2) Tie buttons to change lights pattern functions
                joystick.register_button_handler(change_function_up,
                                                    SixAxis.BUTTON_D_RIGHT)
                joystick.register_button_handler(change_function_down,
                                                    SixAxis.BUTTON_D_LEFT)
                
                         
            while True:
                delay.sleep()
                left = joystick.axes[1].corrected_value()
                mx.drive_left(left*self.max_speed)
                right = joystick.axes[3].corrected_value()
                mx.drive_right(right*self.max_speed)

    def cleanup(self):
        if not self.connection is None:
            self.connection.close()
