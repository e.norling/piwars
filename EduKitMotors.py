import RPi.GPIO as GPIO # Import the GPIO Library

# Set the GPIO modes
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

class Motors:
    # Set variables for the GPIO motor pins
    pinMotorAForwards = 10
    pinMotorABackwards = 9
    pinMotorBForwards = 8
    pinMotorBBackwards = 7

    def __init__(self):
      # How many times to turn the pin on and off each second
      Frequency = 60
      # Settng the duty cycle to 0 means the motors will not turn
    
      # Set the GPIO Pin mode to be Output
      GPIO.setup(self.pinMotorAForwards, GPIO.OUT)
      GPIO.setup(self.pinMotorABackwards, GPIO.OUT)
      GPIO.setup(self.pinMotorBForwards, GPIO.OUT)
      GPIO.setup(self.pinMotorBBackwards, GPIO.OUT)
    
      # Set the GPIO to software PWM at 'Frequency' Hertz
      self.pwmMotorAForwards = GPIO.PWM(self.pinMotorAForwards, Frequency)
      self.pwmMotorABackwards = GPIO.PWM(self.pinMotorABackwards, Frequency)
      self.pwmMotorBForwards = GPIO.PWM(self.pinMotorBForwards, Frequency)
      self.pwmMotorBBackwards = GPIO.PWM(self.pinMotorBBackwards, Frequency)
    
      # Start the software PWM with a duty cycle of 0 (i.e. not moving)
      self.pwmMotorAForwards.start(0)
      self.pwmMotorABackwards.start(0)
      self.pwmMotorBForwards.start(0)
      self.pwmMotorBBackwards.start(0)
  
    # Turn all motors off
    def stop(self):
	self.pwmMotorAForwards.ChangeDutyCycle(0)
	self.pwmMotorABackwards.ChangeDutyCycle(0)
	self.pwmMotorBForwards.ChangeDutyCycle(0)
	self.pwmMotorBBackwards.ChangeDutyCycle(0)

    # Turn both motors forwards
    def forward(self, percent):
	self.drive_left(percent)
	self.drive_right(percent)

    # Turn both motors backwards
    def backward(self, percent):
	self.drive_left(-percent)
	self.drive_right(-percent)

    # Turn Left
    def drive_left(self, percent):
	if (percent > 0):
	    self.pwmMotorBForwards.ChangeDutyCycle(percent)
	    self.pwmMotorBBackwards.ChangeDutyCycle(0)
	elif (percent < 0):
	    self.pwmMotorBForwards.ChangeDutyCycle(0)
	    self.pwmMotorBBackwards.ChangeDutyCycle(-percent)
	else:
	    self.pwmMotorBForwards.ChangeDutyCycle(0)
	    self.pwmMotorBBackwards.ChangeDutyCycle(0)

    # Turn Right
    def drive_right(self, percent):
	if (percent > 0):
	    self.pwmMotorAForwards.ChangeDutyCycle(percent)
	    self.pwmMotorABackwards.ChangeDutyCycle(0)
	elif (percent < 0):
	    self.pwmMotorAForwards.ChangeDutyCycle(0)
	    self.pwmMotorABackwards.ChangeDutyCycle(-percent)
	else:
	    self.pwmMotorAForwards.ChangeDutyCycle(0)
	    self.pwmMotorABackwards.ChangeDutyCycle(0)
