import RPi.GPIO as GPIO # Import the GPIO Library
from triangula.util import IntervalCheck

class Button:
    """
    Creates an event-driven button
    """
    def __init__(self, pin):
        """
        Initialise button

        :param int pin:
            GPIO pin this button is wired to
        """
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.pin = pin

    def add_event(self,this_callback):
        """
        Add an event handler.
        Sets the bouncetime to 500 so that the callback doesn't get called more
        than once for a single button press.
        """
        GPIO.add_event_detect(self.pin, GPIO.RISING, callback=this_callback,
                              bouncetime=1000)

    def rem_events(self):
        """
        Removes all callbacks associated with events on this button
        """
        GPIO.remove_event_detect(self.pin)
