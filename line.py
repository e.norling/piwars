import RPi.GPIO as GPIO # Import the GPIO Library
import time # Import the Time library
from triangula.util import IntervalCheck

class LineFollower:

    def __init__(self, pin):
        #Declare the GPIO settings to use Raspberry pi board pin numbers
        GPIO.setmode(GPIO.BCM)
        self.pin = pin
        GPIO.setup(pin, GPIO.IN)

    # Return True if the line detector is over a black line
    def IsOverBlack(self):
        if GPIO.input(self.pin) == 0:
            return True
        else:
            return False

    def add_event(self,this_callback):
        GPIO.add_event_detect(self.pin, GPIO.BOTH, callback=this_callback)

    def rem_events(self):
        GPIO.remove_event_detect(self.pin)
