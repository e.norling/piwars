import threading
import time
import math

import scrollphathd
from scrollphathd.fonts import font5x7

class ScrollThread(threading.Thread):
    delay = 0.03
    display_on = True
    text = ""

    def __init__(self, string=""):
        threading.Thread.__init__(self)
        self.text = string
        scrollphathd.rotate(180)
        scrollphathd.clear()

        scrollphathd.write_string(self.text, x=0, y=0,
                font=font5x7, brightness=0.5)
        scrollphathd.show()

    def run(self):
        while True:
            if (self.display_on):
                scrollphathd.show()
                scrollphathd.scroll(1)
                time.sleep(self.delay)

    def set_text(self, string):
        scrollphathd.clear()
        self.text = string
        scrollphathd.write_string(self.text, x=0, y=0,
                font=font5x7, brightness=0.5)
        scrollphathd.scroll_to(0,0)
        self.display_on = True

    def stop(self):
        self.display_on = False
        scrollphathd.clear()
        scrollphathd.show()
