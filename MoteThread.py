import threading
import time
import math
import motephat
import webcolors
from colorsys import hsv_to_rgb
from random import randint

pixels = (7,24,0,7)     # number of pixels in each mote strand
numPixels = sum(pixels)

glitterColors = (webcolors.name_to_rgb("OrangeRed"),
                    webcolors.name_to_rgb("DarkOrange"),
                    webcolors.name_to_rgb("Gold"))
numColors = len(glitterColors)

class MoteThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        for channel in range(4):
            motephat.configure_channel(channel+1, pixels[channel], False)
        motephat.set_brightness(1)
        motephat.clear()
        motephat.show()
        self.display_on = False
        self.color = (255, 255, 255)

    def run(self):
        while True:
            if (self.display_on):
                self.update_func()

## Methods to change display function

    def pulse(self, color=None):
        self.update_func = self.pulse_update
        if not color is None:
            self.color = color
        self.display_on = True
        motephat.clear()
        motephat.set_brightness(1.0)
        motephat.show()

    def strobe(self, color=None, strobe_len=5):
        if not color is None:
            self.color = color
        self.strobe_len = strobe_len
        self.start_pos = [strobe_len,strobe_len,strobe_len,strobe_len]
        self.update_func = self.strobe_update
        motephat.clear()
        motephat.set_brightness(1.0)
        motephat.show()
        self.display_on = True

    def rainbow(self):
        self.update_func = self.rainbow_update
        self.display_on = True
        motephat.clear()
        motephat.set_brightness(1.0)
        motephat.show()

    def glitter(self):
        self.update_func = self.glitter_update
        self.display_on = True
        motephat.clear()

        motephat.set_brightness(1.0)
        motephat.show()

    def stop(self):
        self.display_func = motephat.clear
        self.display_on = False
        time.sleep(0.2)
        motephat.clear()
        motephat.show()

    def set_color(self, color):
        self.color = color

## Internal methods to set as display_func

    def pulse_update(self):
        br = (math.sin(time.time()) + 1) / 2
        (r,g,b) = self.color
        r = br*float(r)
        r = int(r)
        g = br*float(g)
        g = int(g)
        b = br*float(b)
        b = int(b)

        for channel in range(4):
            for pixel in range(pixels[channel]):
                motephat.set_pixel(channel+1, pixel, r, g, b)
        motephat.show()

    def rainbow_update(self):
        h = time.time() * 50
        for channel in range(4):
            for pixel in range(pixels[channel]):
                hue = (h + (channel * 64) + (pixel * 4)) % 360
                r, g, b = [int(c * 255) for c in hsv_to_rgb(hue/360.0, 1.0, 1.0)]
                motephat.set_pixel(channel + 1, pixel, r, g, b)
        motephat.show()
        time.sleep(0.01)

    def strobe_update(self):
        (r,g,b) = self.color
        br = 1.0/self.strobe_len
        brightness = 1.0
        for x in range(self.strobe_len+1):
            for channel in range(4):
                if (pixels[channel] > 0):
                    pos = (self.start_pos[channel]-x)%pixels[channel]
                    if (pos > 0):
                        motephat.set_pixel(channel+1, pos, r, g, b, brightness)
            brightness -= br
        for channel in range(4):
            if (pixels[channel] > 0):
                pos = self.start_pos[channel]-self.strobe_len
                self.start_pos[channel] = (self.start_pos[channel]+1)%pixels[channel]
        motephat.show()

    def glitter_update(self):
        pixels_on = [randint(0, numPixels-1)]
        for i in range(10):
            pixels_on.append(randint(0, numPixels-1))

        for channel in range(4):
            def light(x):
                if (x >= 0 and x < pixels[channel]):
                    (r,g,b) = glitterColors[randint(0,numColors-1)]
                    motephat.set_pixel(channel+1, x, r, g, b)
            map(light, pixels_on)
            pixels_on = [pixel - pixels[channel] for pixel in pixels_on]
        motephat.show()
