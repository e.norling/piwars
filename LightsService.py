import rpyc
import threading
import time
import webcolors
from ScrollThread import ScrollThread
from MoteThread import MoteThread

import scrollphathd
from scrollphathd.fonts import font5x7

class LightsService(rpyc.Service):
    def on_connect(self):
        self.scrollThread = ScrollThread("Glitterator ")
        self.scrollThread.start()
        self.moteThread = MoteThread()
        self.moteThread.start()
        pass

    def on_disconnect(self):
        self.scrollThread.stop()
        self.moteThread.stop()

    def exposed_write(self, string):
        self.scrollThread.stop()
        self.scrollThread.set_text(string)

    def exposed_rainbow(self):
        self.moteThread.rainbow()

    def exposed_pulse(self, color=None):
        if not color is None:
            rgb_color = webcolors.name_to_rgb(color)
            self.moteThread.pulse(rgb_color)
        else:
            self.moteThread.pulse()

    def exposed_strobe(self, color=None, length=5):
        if not color is None:
            rgb_color = webcolors.name_to_rgb(color)
            self.moteThread.strobe(rgb_color, length)
        else:
            self.moteThread.strobe(color, length)

    def exposed_glitter(self):
        self.scrollThread.set_text("All that glitters is not gold ")
        self.moteThread.glitter()

    def exposed_stop_scroll(self):
        self.scrollThread.stop()

    def exposed_stop_motes(self):
        self.moteThread.stop()

    def exposed_stop(self):
        self.scrollThread.stop()
        self.moteThread.stop()

    def exposed_set_color(self, color):
        rgb_color = webcolors.name_to_rgb(color)
        self.moteThread.set_color(rgb_color)

try:
    if __name__ == "__main__":
        from rpyc.utils.server import ThreadedServer
        t = ThreadedServer(LightsService, port = 18861)
        t.start()

except KeyboardInterrupt:
    self.scrollThread.stop()
    self.moteThread.stop()
    time.sleep(0.5)
