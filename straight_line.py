import time
import sys
import rpyc
import socket
import VL53L0X
import RPi.GPIO as GPIO
from legomotors import Motors
from button import Button
from triangula.util import IntervalCheck

mx = Motors(100, 1, 2, 3)

WIDTH = 534         # distance between walls (mm)
LEFT_DISTANCE = 200 # aim to stay this distance from the wall
MARGIN = 30         # margin for alignment with wall
START_SPEED = 40    # Start from this speed (%)
MAX_SPEED = 100     # Never go faster than this speed (%)
SPEED_INC = 0.10    # Rate at which to increase speed
MAX_INCS = 9
TURN_SPEED = 40
DENOMINATOR = 20.0
SENSITIVITY = 10     # measurement sensitivity (mm)
TIMING = 100        # min time between measurements (ms)
SENSOR_SPACE = 100  # distance (mm) between sensors
STOP_MARGIN = 500   # stop barrier will be at least this far from end of course

def setup_back(channel):
    """Callback for adding the back sensor.
    Will attempt to initialise this sensor when the button is pressed.
    """
    global back_connected
    if (not back_connected):
        back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        back_connected = True

def setup_front(channel):
    """Callback for adding the back sensor.
    Will attempt to initialise this sensor when the button is pressed.
    """
    global front_connected
    if (not front_connected):
        front_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        front_connected = True

def go_button(channel):
    """Callback button for after robot is ready to run.
    Will start/stop the robot.
    """
    global should_go, left_speed, right_speed
    should_go = not should_go
    if (should_go):
        if not c is None:
            c.root.pulse("green")
        print "***Starting run***"
        # start relatively slowly; jumping straight to max speed makes us turn
        left_speed = START_SPEED
        right_speed = START_SPEED
    else:
        print "***Stopping run***"
        if not c is None:
            c.root.pulse("DarkOrange")
        mx.stop()

def adjust_direction():
    """Work out what direction we should be heading and set the
    two motor speeds accordingly.
    """
    global left_speed, right_speed
    prev_max = max(left_speed, right_speed, START_SPEED)
    print ("Prev max was %d" % (prev_max))

    left_distance = left_tof.get_distance()
    front_distance = front_tof.get_distance()
    back_distance = back_tof.get_distance()
    print ("%dmm from wall in front, left front is %dmm, left back is %dmm"
            % (front_distance, left_distance, back_distance))
    go_straight(front_distance, left_distance, back_distance, prev_max)

##    if (front_distance < STOP_MARGIN):
##        # finished run; slow down and stop
##        left_speed = 0
##        right_speed = 0
##        return

def go_straight(front_distance,lf_distance, lb_distance, prev_max):
    global left_speed, right_speed

    # Should turn more sharply the closer we are to a wall 
    if (lf_distance < LEFT_DISTANCE-MARGIN):
        # Too close to the wall; move back to comfort zone
        diff = lf_distance - lb_distance
        if (diff > SENSITIVITY):
            left_speed = prev_max
            right_speed = prev_max
            print ("Too close to wall but already heading right")
        else:
            factor = abs(diff)/DENOMINATOR
            print ("factor is %f"%(factor))
            if (factor > 1):
                factor = 1
            factor = int(factor*MAX_INCS)+1
            left_speed = (1+factor*SPEED_INC)*START_SPEED
            right_speed = START_SPEED
            print ("Too close, turning right")
    elif (lf_distance > LEFT_DISTANCE+MARGIN):
        # Too far from wall; move back to comfort zone
        diff = lb_distance - lf_distance
        if (diff > SENSITIVITY and lb_distance < WIDTH):
            left_speed = prev_max
            right_speed = prev_max
            print("Too far from wall, but already heading left")
        else:
            if (lf_distance > 2*LEFT_DISTANCE):
                if (left_speed > 0 or right_speed > 0):
                    print "stop"
                    left_speed = 0
                    right_speed = 0
                else:
                    print "turn left"
                    left_speed = -TURN_SPEED
                    right_speed = TURN_SPEED
                    mx.drive(left_speed, right_speed)
            else:
                factor = abs(diff)/DENOMINATOR
                print ("factor is %f"%(factor))
                if (factor > 1):
                    factor = 1
                factor = int(factor*MAX_INCS)+1
                left_speed = START_SPEED
                right_speed = (1+factor*SPEED_INC)*START_SPEED
            print ("Too far, turning left")
    else:
        straighten_up(front_distance, lf_distance, lb_distance, prev_max)

def straighten_up(front_distance, lf_distance, lb_distance, prev_max):
    global left_speed, right_speed

    # In comfort zone; aim to head straight
    diff = abs(lf_distance-lb_distance)
    # since we're in comfort zone, speed up (gradually) if we can
    print ("Difference between sensor readings is %d" % (diff))

    if (diff > SENSITIVITY):
        if (lf_distance > lb_distance):
            # currently heading right
            if (left_speed == 0):
                left_speed = START_SPEED
                right_speed = START_SPEED
            slower = (1-SPEED_INC)*left_speed
            if (slower > right_speed):
                left_speed = slower
        else:
            # currently heading left
            if (left_speed == 0):
                left_speed = START_SPEED
                right_speed = START_SPEED
            slower = (1-SPEED_INC)*right_speed
            if (slower > left_speed):
                right_speed = slower
        print "In comfort zone, straightening up"
    else:
        max_speed = MAX_SPEED
        if (front_distance < WIDTH):
            max_speed /=2
        speed = min(max_speed, (1+SPEED_INC)*prev_max)
        print ("New max speed is %f" %(speed))
        left_speed = speed
        right_speed = speed
        print "In comfort zone, speeding up and heading straight"


try:
    try:
        c = rpyc.connect("glitter.local", 18861)
    except socket.gaierror as e:
        c = None
        
    if not c is None:
        c.root.pulse("red")
    left_tof = VL53L0X.VL53L0X(address=0x2B)
    back_tof = VL53L0X.VL53L0X(address=0x2D)
    front_tof = VL53L0X.VL53L0X(address=0x2F)
    left_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    timing = left_tof.get_timing()/1000
    if (timing < TIMING):
        timing = TIMING
    print ("Timing %d ms" % (timing))
    left_distance = left_tof.get_distance()
    if (left_distance < 0):
        print ("Problem with distance measurement from sensor %d (%d); aborting"
               % (left_tof.my_object_number, left_distance))
        GPIO.cleanup()
        sys.exit()

    should_go = False
    left_speed = START_SPEED
    right_speed = START_SPEED
    switch_pin = 27
    button = Button(switch_pin)

    back_connected = False
    front_connected = False

    button.add_event(setup_back)
    print "Press button when back sensor connected"
    # wait until back left sensor added
    while (not back_connected):
        pass
    # Back sensor connected, remove that event handler
    button.rem_events()

    # Make sure we can get a proper reading from the back sensor
    back_distance = back_tof.get_distance()
    if (back_distance < 0):
        print ("Problem with distance measurement from sensor %d (%d); aborting"
               % (back_tof.my_object_number, back_distance))
        GPIO.cleanup()
        sys.exit()

    button.add_event(setup_front)
    print "Press button when front sensor connected"
    # wait until front sensor added
    while (not front_connected):
        pass
    # All sensors connected, remove that event handler
    button.rem_events()

    # Make sure we can get a proper reading from the front sensor
    front_distance = front_tof.get_distance()
    if (front_distance < 0):
        print ("Problem with distance measurement from sensor %d (%d); aborting"
               % (front_tof.my_object_number, front_distance))
        GPIO.cleanup()
        sys.exit()

    # Now get ready to go
    button.add_event(go_button)
    print "Set up complete, press button to go"
    
    if not c is None:
        c.root.pulse("DarkOrange")
        
    padding = IntervalCheck(interval=timing/1000.0)
    while(True):
        with padding:
            if (should_go):
                adjust_direction()
                print ("Motor left %.2f motor right %.2f"
                        % (left_speed, right_speed))
                mx.drive(left_speed, right_speed)

except KeyboardInterrupt:
    mx.stop()
    GPIO.cleanup()
