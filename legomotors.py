import RPi.GPIO as GPIO
import time

#Declare the GPIO settings
# to use Raspberry pi board pin numbers
GPIO.setmode(GPIO.BCM)

class Motors:
    """
    Resource class to handle lego connection motors
    """
    # Set variables for the GPIO motor pins
    # These are wired up in the hardware configuration
    # connecting to the four motor connectors
    # (M4 is nearest the USB ports)
    M1PWM = 22
    M1F = 23
    M1B = 24
    M2PWM = 7
    M2F = 8
    M2B = 25
    M3PWM = 5
    M3F = 12
    M3B = 6
    M4PWM = 16
    M4F = 13
    M4B = 19

    # Drive the right motor a bit harder than the left to compensate
    # for the fact that the gears sometimes skip
    LEFT_MAX = 0.85
    RIGHT_MAX = 1

    def __init__(self, frequency, left, right, flicker):
        """
        Initialise the motor controller.

        :param int frequency:
            Frequency at which to run the motors.
        :param int left:
            Which connector the left motor is on
        :param int right:
            Which connector the right motor is on
        """
        if (left == 1):
            self.MLF = self.M1F
            self.MLB = self.M1B
            self.MLPWM = self.M1PWM
        elif (left == 2):
            self.MLF = self.M2F
            self.MLB = self.M2B
            self.MLPWM = self.M2PWM
        elif (left == 3):
            self.MLF = self.M3F
            self.MLB = self.M3B
            self.MLPWM = self.M3PWM
        elif (left == 4):
            self.MLF = self.M4F
            self.MLB = self.M4B
            self.MLPWM = self.M4PWM
        if (right == 1):
            self.MRF = self.M1F
            self.MRB = self.M1B
            self.MRPWM = self.M1PWM
        elif (right == 2):
            self.MRF = self.M2F
            self.MRB = self.M2B
            self.MRPWM = self.M2PWM
        elif (right == 3):
            self.MRF = self.M3F
            self.MRB = self.M3B
            self.MRPWM = self.M3PWM
        elif (right == 4):
            self.MRF = self.M4F
            self.MRB = self.M4B
            self.MRPWM = self.M4PWM
        if (flicker == 1):
            self.MHF = self.M1F
            self.MHB = self.M1B
            self.MHPWM = self.M1PWM
        elif (flicker == 2):
            self.MHF = self.M2F
            self.MB = self.M2B
            self.MHPWM = self.M2PWM
        elif (flicker == 3):
            self.MHF = self.M3F
            self.MHB = self.M3B
            self.MHPWM = self.M3PWM
        elif (flicker == 4):
            self.MHF = self.M4F
            self.MHB = self.M4B
            self.MHPWM = self.M4PWM

        # set up GPIO pins
        GPIO.setup(self.MLPWM, GPIO.OUT)
        GPIO.setup(self.MLF, GPIO.OUT)
        GPIO.setup(self.MLB, GPIO.OUT)
        GPIO.setup(self.MRPWM, GPIO.OUT)
        GPIO.setup(self.MRF, GPIO.OUT)
        GPIO.setup(self.MRB, GPIO.OUT)
        GPIO.setup(self.MHPWM, GPIO.OUT)
        GPIO.setup(self.MHF, GPIO.OUT)
        GPIO.setup(self.MHB, GPIO.OUT)

        GPIO.setup(26, GPIO.OUT)
        GPIO.output(26, GPIO.HIGH)

        # set up PWM
        self.pwmML = GPIO.PWM(self.MLPWM, frequency)
        self.pwmMR = GPIO.PWM(self.MRPWM, frequency)
        self.pwmMH = GPIO.PWM(self.MHPWM, frequency)
        self.pwmML.start(0)
        self.pwmMR.start(0)
        self.pwmMH.start(0)
        self.pwmMH.ChangeDutyCycle(0)

    def drive_left(self,percent):
        """
        Drive the left motor.

        :param int percent:
            Percentage of max speed to drive the motor
        """
	if (percent > 0) :
	    #Set the direction of motor forward
	    GPIO.output(self.MLF, GPIO.HIGH)
	    GPIO.output(self.MLB, GPIO.LOW)
	elif (percent < 0) :
	    #Set the direction of motor backward
	    GPIO.output(self.MLF, GPIO.LOW)
	    GPIO.output(self.MLB, GPIO.HIGH)
	    percent = -percent
	else :
	    #Stop motor
	    GPIO.output(self.MLF, GPIO.LOW)
	    GPIO.output(self.MLB, GPIO.LOW)

	#Set the Speed / PWM for motor
	self.pwmML.ChangeDutyCycle(self.LEFT_MAX*percent)
      
    def drive_right(self,percent):
        """
        Drive the right motor.

        :param int percent:
            Percentage of max speed to drive the motor
        """
	if (percent > 0) :
	    #Set the direction of motor forward
	    GPIO.output(self.MRF, GPIO.HIGH)
	    GPIO.output(self.MRB, GPIO.LOW)
	elif (percent < 0) :
	    #Set the direction of motor backward
	    GPIO.output(self.MRF, GPIO.LOW)
	    GPIO.output(self.MRB, GPIO.HIGH)
	    percent = -percent
	else :
	    #Stop motor
	    GPIO.output(self.MRF, GPIO.LOW)
	    GPIO.output(self.MRB, GPIO.LOW)

	#Set the Speed / PWM for motor
	self.pwmMR.ChangeDutyCycle(self.RIGHT_MAX*percent)

    def drive(self, left_percent, right_percent):
        """
            Drive the robot.

            :param int left_percent:
                Percentage of max speed to drive left motor
            :param int right_percent:
                Percentage of max speed to drive right motor
        """
        self.drive_left(left_percent)
        self.drive_right(right_percent)
	
    def forward(self,percent):
        self.drive_left(percent)
        self.drive_right(percent)

    def backward(self,percent):
        self.drive_left(-percent)
        self.drive_right(-percent)

    def flicker_forward(self, percent):
        if (percent > 0) :
            #Set the direction of motor forward
            GPIO.output(self.MHB, GPIO.HIGH)
            GPIO.output(self.MHF, GPIO.LOW)
        elif (percent < 0) :
            #Set the direction of motor backward
            GPIO.output(self.MHB, GPIO.LOW)
            GPIO.output(self.MHF, GPIO.HIGH)
            percent = -percent
        else :
            #Stop motor
            GPIO.output(self.MHF, GPIO.LOW)
            GPIO.output(self.MHB, GPIO.LOW)

        #Set the Speed / PWM for motor
        self.pwmMH.ChangeDutyCycle(percent)

    def flicker_backward(self, percent):
        flicker_forward(-percent)

    def flicker_stop(self):
        self.flicker_forward(0)
        self.pwmMH.ChangeDutyCycle(0)

    def stop(self):
        """Set everything to low (Switch everything Off)
        """
        GPIO.output(self.MLB, GPIO.LOW)
        GPIO.output(self.MLF, GPIO.LOW)
        self.pwmML.ChangeDutyCycle(0)
        GPIO.output(self.MRF, GPIO.LOW)
        GPIO.output(self.MRB, GPIO.LOW)
        self.pwmMR.ChangeDutyCycle(0)
