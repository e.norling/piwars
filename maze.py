import time
import sys
import rpyc
import socket
import VL53L0X
import RPi.GPIO as GPIO
from legomotors import Motors
from button import Button
from triangula.util import IntervalCheck

mx = Motors(100, 1, 2, 3)

WIDTH = 350         # distance between walls (mm)
LEFT_DISTANCE = 80  # Want to keep wall roughly this far away on LHS (mm)
MARGIN = 10         # amount allowed to stray from desired distance from wall (mm)
START_SPEED = 40    # Start from this speed (%)
MAX_SPEED = 100     # Never go faster than this speed (%)
SPEED_INC = 0.05    # Rate at which to increase speed
MAX_INCS = 9        
SENSITIVITY = 5     # measurement sensitivity (5mm)
MEGA_SENSITIVITY = 20
SENSOR_SPACE = 100  # distance (mm) between sensors
TURN_SPEED = 40     # speed at which to turn (one side goes - this to turn on spot) (%)
FAST_TURN = 65
SLOW_TURN = 25
TIMING = 100        # min time between measurements (ms)
MAX_READ = 8190     # largest reading from sensors
FRONT_MARGIN = 1.9*LEFT_DISTANCE

# Sensor setup callbacks. Assumes left front sensor is already attached when
# script starts running. Needed because we don't have the ability to wire up
# all the pins on the sensors. So instead we physically plug them in one by
# one. Clunky but seems to work!

"""
Sensor positions should be as follows:

          F

       X------X
    LF X      X
       X      X
       X+----+X
       X|    |X
    LB X+----+X
       X      X
(The two sensor on left side allow checking when robot is straight)
"""

def setup_front(channel):
    """Callback for adding the front sensor.
    Will attempt to initialise this sensor when the button is pressed.
    """
    global front_connected
    if (not front_connected):
        front_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        front_connected = True

def setup_lb(channel):
    """Callback for adding the back sensor.
    Will attempt to initialise this sensor when the button is pressed.
    """
    global lb_connected
    if (not lb_connected):
        lb_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        lb_connected = True

# Once sensors are attached, repurpose button to start/stop maze navigation

def go_button(channel):
    """Callback button for after robot is ready to run.
    Will start/stop the robot.
    """
    global should_go, left_speed, right_speed
    should_go = not should_go
    if (should_go):
        print "***Starting the maze"
        if not c is None:
            c.root.rainbow()
        # start relatively slowly; jumping straight to max speed makes us turn
        left_speed = START_SPEED
        right_speed = START_SPEED
        direction = STRAIGHT
        check_position()
    else:
        print "***Stopping"
        left_speed = 0
        right_speed = 0
        mx.stop()
        if not c is None:
            c.root.strobe("yellow", 5)


STRAIGHT = 1
LEFT = 2
ALIGNING = 3
def check_position():
    """Work out what direction we should be heading and set the
    two motor speeds accordingly.
    """
    global direction, front_distance, lf_distance, lb_distance
    global left_speed, right_speed, cleared

    prev_max = max(left_speed, right_speed, START_SPEED)
    print ("Prev max was %d" % (prev_max))
    prev_front = front_distance
    prev_lf = lf_distance
    prev_lb = lb_distance

    front_distance = front_tof.get_distance()
    if (front_distance == MAX_READ):
            front_distance = front_tof.get_distance()
            
    lf_distance = lf_tof.get_distance()
    if (lf_distance == MAX_READ):
        lf_distance = lf_tof.get_distance()

    lb_distance = lb_tof.get_distance()
    if (lb_distance == MAX_READ):
        lb_distance = lb_tof.get_distance()

    print ("front is %dmm from wall, left front is %dmm, left back is %dmm"
            % (front_distance, lf_distance, lb_distance))
    
    if (lf_distance > WIDTH and not (front_distance == MAX_READ) and direction == STRAIGHT):
        # Need to get around the sticky-out bit
        direction = LEFT
        cleared = False
    if (direction == LEFT):
        print "turning left"
        turn_left(front_distance, lf_distance, lb_distance, prev_max)
    elif (direction == STRAIGHT):
        if (front_distance < FRONT_MARGIN
              or (front_distance < WIDTH and lf_distance < LEFT_DISTANCE-MARGIN)):
            # need to stop and change direction to follow the wall currently in front
            if (left_speed > 0 or right_speed > 0):
                print "stop"
                left_speed = 0
                right_speed = 0
            else:
                print "turn right"
                left_speed = TURN_SPEED
                right_speed = -TURN_SPEED
                mx.drive(left_speed, right_speed)
        else:
            go_straight(front_distance, lf_distance, lb_distance, prev_max)

def turn_left(front_distance, lf_distance, lb_distance, prev_max):
    global left_speed, right_speed
    global direction, cleared

    # turn until left front can see wall on left
    if (lf_distance < LEFT_DISTANCE+MARGIN):
        # reached target
        left_speed = 0
        right_speed = 0
        direction = STRAIGHT
        print "Reached target"
        #GPIO.cleanup()
        #sys.exit()
    else:
        left_speed = SLOW_TURN
        right_speed = FAST_TURN
        print "Turning left"
        
def go_straight(front_distance,lf_distance, lb_distance, prev_max):
    global left_speed, right_speed

    # Should turn more sharply the closer we are to a wall 
    if (lf_distance < LEFT_DISTANCE-MARGIN):
        # Too close to the wall; move back to comfort zone
        diff = lf_distance - lb_distance
        if (diff > SENSITIVITY):
            left_speed = prev_max
            right_speed = prev_max
            print ("Too close to wall but already heading right")
        else:
            factor = abs(diff)/10.0
            print ("factor is %f"%(factor))
            if (factor > 1):
                factor = 1
            factor = int(factor*MAX_INCS)+1
            left_speed = (1+factor*SPEED_INC)*START_SPEED
            right_speed = START_SPEED
            print ("Too close, turning right")
    elif (lf_distance > LEFT_DISTANCE+MARGIN):
        # Too far from wall; move back to comfort zone
        diff = lb_distance - lf_distance
        if (diff > SENSITIVITY and lb_distance < WIDTH):
            left_speed = prev_max
            right_speed = prev_max
            print("Too far from wall, but already heading left")
        else:
            if (lf_distance > 2*LEFT_DISTANCE):
                if (left_speed > 0 or right_speed > 0):
                    print "stop"
                    left_speed = 0
                    right_speed = 0
                else:
                    print "turn left"
                    left_speed = -TURN_SPEED
                    right_speed = TURN_SPEED
                    mx.drive(left_speed, right_speed)
            else:
                factor = abs(diff)/10.0
                print ("factor is %f"%(factor))
                if (factor > 1):
                    factor = 1
                factor = int(factor*MAX_INCS)+1
                left_speed = START_SPEED
                right_speed = (1+factor*SPEED_INC)*START_SPEED
            print ("Too far, turning left")
    else:
        straighten_up(front_distance, lf_distance, lb_distance, prev_max)

def straighten_up(front_distance, lf_distance, lb_distance, prev_max):
    global left_speed, right_speed

    # In comfort zone; aim to head straight
    diff = abs(lf_distance-lb_distance)
    # since we're in comfort zone, speed up (gradually) if we can
    print ("Difference between sensor readings is %d" % (diff))

    if (diff > SENSITIVITY):
        if (lf_distance > lb_distance):
            # currently heading right
            if (left_speed == 0):
                left_speed = START_SPEED
                right_speed = START_SPEED
            slower = (1-SPEED_INC)*left_speed
            if (slower > right_speed):
                left_speed = slower
        else:
            # currently heading left
            if (left_speed == 0):
                left_speed = START_SPEED
                right_speed = START_SPEED
            slower = (1-SPEED_INC)*right_speed
            if (slower > left_speed):
                right_speed = slower
        print "In comfort zone, straightening up"
    else:
        max_speed = MAX_SPEED
        if (front_distance < WIDTH):
            max_speed /=2
        speed = min(max_speed, (1+SPEED_INC)*prev_max)
        print ("New max speed is %f" %(speed))
        left_speed = speed
        right_speed = speed
        print "In comfort zone, speeding up and heading straight"

                
try:
    lf_tof = VL53L0X.VL53L0X(address=0x2B)
    front_tof = VL53L0X.VL53L0X(address=0x2F)
    lb_tof = VL53L0X.VL53L0X(address=0x31)
    c = None
    try:
        c = rpyc.connect("glitter.local", 18861)
    except socket.gaierror as e:
        pass

    if not c is None:
        c.root.strobe("red", 5)

    # Assume left front sensor was already attached when script started
    lf_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    timing = lf_tof.get_timing()/1000
    if (timing < TIMING):
        timing = TIMING
    print ("Timing %d ms" % (timing))
    lf_distance = lf_tof.get_distance()
    if (lf_distance < 0):
        print ("Problem with distance measurement from sensor %d (%d); aborting"                % (lf_tof.my_object_number, lf_distance))
        GPIO.cleanup()
        sys.exit()

    # Now add each sensor in turn, adjusting i2c address as each is added
    should_go = False
    switch_pin = 27
    button = Button(switch_pin)

    front_connected = False
    lb_connected = False
    left_speed = 0
    right_speed = 0

    button.add_event(setup_lb)
    print "Press button when left back sensor connected"
    # wait until left sensor added
    while (not lb_connected):
        pass
    # Make sure we can get a proper reading from the back sensor
    lb_distance = lb_tof.get_distance()
    if (lb_distance < 0):
        print ("Problem with distance measurement from sensor %d (%d); aborting"% (lb_tof.my_object_number, lb_distance))
        GPIO.cleanup()
        sys.exit()
    button.rem_events()

    button.add_event(setup_front)
    print "Press button when front sensor connected"
    # wait until front sensor added
    while (not front_connected):
        pass
    # Make sure we can get a proper reading from the front sensor
    front_distance = front_tof.get_distance()
    if (front_distance < 0):
        print ("Problem with distance measurement from sensor %d (%d); aborting"% (front_tof.my_object_number, front_distance))
        GPIO.cleanup()
        sys.exit()
    button.rem_events()

    button.add_event(setup_lb)
    print "Press button when left back sensor connected"
    # wait until left sensor added
    while (not lb_connected):
        pass
    # Make sure we can get a proper reading from the back sensor
    lb_distance = lb_tof.get_distance()
    if (lb_distance < 0):
        print ("Problem with distance measurement from sensor %d (%d); aborting"                % (lb_tof.my_object_number, lb_distance))
        GPIO.cleanup()
        sys.exit()

    # Now get ready to go
    button.rem_events()

    if not c is None:
        c.root.strobe("green", 5)

    button.add_event(go_button)
    print "Set up complete, press button to go"
    
    # All set up, enter driving loop
    # Assume set up to head straight to start
    direction = STRAIGHT
    padding = IntervalCheck(interval=timing/1000.0)
    while(True):
        with padding:
            if (should_go):
                check_position()
                print ("Motor left %.2f motor right %.2f"
                        % (left_speed, right_speed))
                mx.drive(left_speed, right_speed)

except KeyboardInterrupt:
    if not c is None:
        c.close()
    mx.stop()
    GPIO.cleanup()

